package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.uib.ii.inf112.pond.Pond;

public class TestTest {
	TextAligner aligner = new TextAligner() {

		public String center(String text, int width) {
			
			testWidth(width, text.length());

			int extra = (width - text.length()) / 2;
			return " ".repeat(extra) + text + " ".repeat(extra);
		}

		public String flushRight(String text, int width) {
			
			testWidth(width, text.length());

			int extra = width - text.length();

			return " ".repeat(extra) + text;
		}

		public String flushLeft(String text, int width) {
			
			testWidth(width, text.length());

			int extra = width - text.length();

			return text + " ".repeat(extra);
		}

		public String justify(String text, int width) {

			testWidth(width, text.length());

			String[] splitStr = text.trim().split("\\s+");

			if (splitStr.length == 1)
				return center(text, width);

			int strLen = 0;
			
			for (int i = 0; i < splitStr.length; i++) {
				strLen += splitStr[i].length();
			}

			int spaces = width - strLen;

			int div = spaces / (splitStr.length-1);

			if (spaces % (splitStr.length-1) != 0)
				div++;
			

			String finalStr = "";

			for (int i = 0; i < splitStr.length; i++) {
				finalStr += splitStr[i];

				if (i != splitStr.length-1)
					finalStr += " ".repeat(div);
			}
			
			return finalStr;
			}
		};	

		private void testWidth (int width, int textLength) {
			
			if (width <= 0)
				throw new IllegalArgumentException("Width must be at least 1");
			
			if (width < textLength)
				throw new IllegalArgumentException("Width cannot be less than the string's length");
			
		}
		
	Pond testPond;

	@BeforeEach
	void setUp() {
		testPond = new Pond(1280, 720);
	}
	
	@Test
	void duckExistsTest() {
		assertEquals(1, testPond.objs.size());
	}

	@Test
	void testOneDuck25Steps() {
		for (int i = 1; i < 25; i++) {
			testPond.step();
			assertEquals(1, testPond.objs.size());
		}
	}

	@Test
	void test2425Steps() {
		for (int i = 1; i < 25; i++) {
			testPond.step();
		}
		assertEquals(1, testPond.objs.size());

		testPond.step();

		assertEquals(2, testPond.objs.size());
	}


	@Test
	void testMoveFunction() {
		testPond.objs.get(0).moveTo(25, -72);

		assertEquals(25, testPond.objs.get(0).getX());
		assertEquals(-72, testPond.objs.get(0).getY());

	}


	@Test
	void testCorrectMovement() {
		for (int i = 1; i < 50; i++) {
			testPond.step();
		}
		assertEquals(-50, testPond.objs.get(0).getX());
	}

	
	@Test
	void test() {
		fail("Not yet implemented");
	}

	@Test
	void testCenter() {

		assertThrows(IllegalArgumentException.class, () -> {aligner.center("B", -1);});
		assertThrows(IllegalArgumentException.class, () -> {aligner.center("B", 0);});
		assertThrows(IllegalArgumentException.class, () -> {aligner.center("ABCD", 3);});

		assertEquals("  A  ", aligner.center("A", 5));
		assertEquals(" foo ", aligner.center("foo", 5));
		assertEquals("A", aligner.center("A", 1));
		assertEquals("  1  ", aligner.center(" 1 ", 5));
		assertEquals("   A   ", aligner.center("A", 8));
		assertEquals(" a  ", aligner.center("a ", 5));
	}

	@Test
	void testFlushRight() {
		assertThrows(IllegalArgumentException.class, () -> {aligner.flushRight("B", -1);});
		assertThrows(IllegalArgumentException.class, () -> {aligner.flushRight("B", 0);});
		assertThrows(IllegalArgumentException.class, () -> {aligner.flushRight("ABCD", 3);});

		assertEquals("    A", aligner.flushRight("A", 5));
		assertEquals("  foo", aligner.flushRight("foo", 5));
		assertEquals("A", aligner.flushRight("A", 1));
		assertEquals("     A", aligner.flushRight("A", 6));
		assertEquals("   a ", aligner.flushRight("a ", 5));
	}

	@Test
	void testFlushLeft() {
		assertThrows(IllegalArgumentException.class, () -> {aligner.flushLeft("B", -1);});
		assertThrows(IllegalArgumentException.class, () -> {aligner.flushLeft("B", 0);});
		assertThrows(IllegalArgumentException.class, () -> {aligner.flushLeft("ABCD", 3);});

		assertEquals("A    ", aligner.flushLeft("A", 5));
		assertEquals("foo  ", aligner.flushLeft("foo", 5));
		assertEquals("A", aligner.flushLeft("A", 1));
		assertEquals("A     ", aligner.flushLeft("A", 6));
		assertEquals(" a   ", aligner.flushLeft(" a", 5));
	}

	@Test
	void testJustify() {
		assertThrows(IllegalArgumentException.class, () -> {aligner.justify("B", -1);});
		assertThrows(IllegalArgumentException.class, () -> {aligner.justify("B", 0);});
		assertThrows(IllegalArgumentException.class, () -> {aligner.justify("ABCD", 3);});
		assertThrows(IllegalArgumentException.class, () -> {aligner.justify("a B c", 2);});

		assertEquals("a  b  c", aligner.justify("a b c", 7));
		assertEquals("fee   fie   foo", aligner.justify("fee fie foo", 15));
		assertEquals("My      name      is", aligner.justify("My name is", 20));
		assertEquals("Who    is    this", aligner.justify("Who is this", 17));
		assertEquals("a     bc     def     ghijk     lmnopq     rstuvwx", aligner.justify("a bc def ghijk lmnopq rstuvwx", 49));
		assertEquals("a   b   c", aligner.justify(" a b c ", 9));
	}
}
